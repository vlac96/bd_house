class HouseType {
    constructor(data){
        this.name = data.name;
        this.slug = data.slug;
    }
}

export default HouseType;
