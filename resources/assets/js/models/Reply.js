class Reply {
    constructor(data){
        this.id = data.id;
        this.body = data.body;
        this.createdAt = data.created_at;
        this.updatedAt = data.updated_at;
        this.creator = data.creator;
        this.thread = data.thread;
        this.house_id = data.house_id;
        this.house = JSON.parse(JSON.stringify(data.house));
    }
}

export default Reply;