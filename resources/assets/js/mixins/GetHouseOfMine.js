import House from '../models/House';

var getHouseOfMine = {
    methods: {
        getHouseOfMine() {
            var vm = this;

            axios
                .get("/houses/*", {
                    params: {
                        by: vm.$root.username
                    }
                })
                .then(response => {
                    vm.my_houses = [];
                    if (response.data) response.data.forEach(house => {
                        vm.my_houses.push(new House(house));
                    });
                })
                .catch(error => console.log(error));
        }
    }
}

export default getHouseOfMine;