import HouseType from '../models/HouseType';

var getHouseTypesMixin = {
    methods: {
        getHouseTypes(){
            var vm = this;
            axios.get('/housetypes')
                 .then(response => {
                    if (response.data) response.data.forEach(house_type => {
                        vm.house_types.push(new HouseType(house_type));
                    });
                 })
                 .catch(error => console.log(error));
        }
    }
}

export default getHouseTypesMixin;

