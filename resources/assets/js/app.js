import './bootstrap';
import router from './routes';

import isLoggedMixin from './mixins/IsLoggedMixin';
import getChannelsMixin from './mixins/GetChannelsMixin';
import getHouseTypesMixin from './mixins/getHouseTypesMixin';
import getHouseOfMine from './mixins/GetHouseOfMine';

import Path from './models/Path';
import * as VueGoogleMaps from "vue2-google-maps";
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHome, faArrowRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faHome, faArrowRight);

Vue.component('fa-icon', FontAwesomeIcon);

Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyBZSNVzikMMgFM0E6UPm_OQ11jY_IlGD5U",
        libraries: "places" // necessary for places input
    }
});

// Filters

Vue.filter('truncate', function (text, value) {
    if (text.length <= value) return text;
    return text.substring(0, value) + '...';
});

Vue.filter('fromNow', function (date) {
    return moment(date).fromNow();
});

Vue.filter('bannedFor', function (date) {
    let days = -moment().diff(date, 'days');
    if (days > 1000) return 'vĩnh viễn.';
    return days > 1 ? 'trong ' + days + ' ngày' : 'trong ' + days + ' ngày';
});

Vue.filter('capitalize', function (elem) {
    return elem.toUpperCase();
});

// App
const app = new Vue({
    el: '#app',
    router,
    mixins: [isLoggedMixin, getChannelsMixin, getHouseTypesMixin, getHouseOfMine],
    data: {
        user: false,
        path: new Path(),
        channels: [],
        my_houses: [],
        house_types: [],
        searchQuery: '',
        showNavbar: false
    },
    created() {
        this.checkIfLogged()
            .then(response => {
                this.user = response ? response : false;
            })
            .catch(error => console.log(error));

        this.getChannels();
        this.getHouseTypes();
    },

    methods: {
        logout() {
            axios.post('/logout')
                .then(response => {
                    this.setNewCsrfToken(response.data);
                    this.user = false;
                })
                .catch(error => console.log(error));
        },
        setNewCsrfToken(newToken) {
            document.querySelector('meta[name=csrf-token]').content = newToken;
            window.Laravel = {
                csrfToken: document.querySelector('meta[name=csrf-token]').content
            };
            window.axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel.csrfToken;
        }
    },
    computed: {
        username() {
            return this.user ? this.user.username : false;
        }
    },
    watch : {
        user () {
            this.my_houses = [];
            if(this.user) {
                this.getHouseOfMine();
            }
        }
    }
});