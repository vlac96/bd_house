<aside class="menu column is-3">
    <keep-alive>
        <router-link class="button" id="new-thread-button" v-if="user" :to="{path: '/new-thread'}">Đăng bài</router-link>
    </keep-alive>

    <p class="menu-label">
        BÀI VIẾT
    </p>
    <ul class="menu-list">
        <router-link tag="li" to="/" exact>
            <a>
                <span class="icon is-small filter-icons">
                    <i class="fa fa-globe"></i>
                </span>Tất cả bài viết
            </a>
        </router-link>
        <router-link v-if="$root.username" tag="li" :to="{ path: '/threads', query: { by: $root.username }}" exact>
            <a>
                <span class="icon is-small filter-icons">
                    <i class="fa fa-user"></i>
                </span>Bài viết của tôi
            </a>
        </router-link>
        <router-link v-if="$root.username" tag="li" :to="{ path: '/threads', query: { contributed_to: 1 }}" exact>
            <a>
                <span class="icon is-small filter-icons">
                    <i class="fa fa-code-fork"></i>
                </span>Bài viết có tôi phản hồi
            </a>
        </router-link>
        <router-link tag="li" :to="{ path: '/threads', query: { unanswered: 1 }}" exact>
            <a>
                <span class="icon is-small filter-icons">
                    <i class="fa fa-circle-o"></i>
                </span>Bài viết không có phản hồi
            </a>
        </router-link>
        <router-link tag="li" :to="{ path: '/threads', query: { trending: 1 }}" exact>
            <a>
                <span class="icon is-small filter-icons">
                    <i class="fa fa-users"></i>
                </span>Bài viết phổ biến tuần
            </a>
        </router-link>
        <router-link tag="li" :to="{ path: '/threads', query: { popular: 1 }}" exact>
            <a>
                <span class="icon is-small filter-icons">
                    <i class="fa fa-users"></i>
                </span>Bài viết phổ biến
            </a>
        </router-link>
    </ul>
    <br>
    <br>
    <p class="menu-label">
        LOẠI NHÀ CHO THUÊ
    </p>
    <ul class="menu-list">
        <router-link v-for="(houseType, index) in house_types" tag="li" :to="{ name: 'house_type', params: { house_type: houseType.slug }}"
            :key="index">
            <a>
                <span class="icon is-small filter-icons">
                    <i class="fa fa-asterisk"></i>
                </span>@{{houseType.name}}
            </a>
        </router-link>
    </ul>
    <br>
    <br>
    <p class="menu-label">
        CHỦ ĐỀ
    </p>
    <ul class="menu-list">
        <router-link v-for="(channel, index) in channels" tag="li" :to="{ name: 'channel', params: { channel: channel.slug}}" :key="index">
            <a>
                <span class="icon is-small filter-icons">
                    <i class="fa fa-asterisk"></i>
                </span>@{{channel.name}}
            </a>
        </router-link>
    </ul>

</aside>