<section class="hero is-primary">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        BOARDING HOUSE
      </h1>
      <h2 class="subtitle">
        Nhà trọ của người Việt
      </h2>
    </div>
  </div>
</section>