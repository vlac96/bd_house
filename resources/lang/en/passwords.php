<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Mật khẩu tối thiểu 6 ký tự và trùng với xác nhận mật khẩu.',
    'reset' => 'Mật khẩu của bạn đã được tạo mới!',
    'sent' => 'Đã gửi email tạo mới mật khẩu!',
    'token' => 'Password reset token không hợp lệ.',
    'user' => "Chúng tôi không tìm thấy email do bạn cung cấp.",

];
