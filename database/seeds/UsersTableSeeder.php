<?php

use Illuminate\Database\Seeder;
use \App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'forumAdmin',
            'email' => 'forumAdmin@example.com',
            'password' => bcrypt('password'),
            'isAdmin' => true
        ]);

        User::create([
            'username' => 'User',
            'email' => 'user@example.com',
            'password' => bcrypt('password'),
            'isAdmin' => false
        ]);

        User::create([
            'username' => 'User Test',
            'email' => 'user_test@example.com',
            'password' => bcrypt('password'),
            'isAdmin' => false
        ]);

        factory(User::class, 30)->create();

    }
}
