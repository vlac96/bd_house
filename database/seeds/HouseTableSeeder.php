<?php

use Illuminate\Database\Seeder;
use App\House;

class HouseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(House::class, 20)->create();
    }
}
