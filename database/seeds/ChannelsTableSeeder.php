<?php

use Illuminate\Database\Seeder;
use \App\Channel;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Channel::create([
            'name' => 'Cho thuê',
            'description' => 'Cho thuê nhà trọ, nhà nghỉ, ...',
            'slug' => 'cho-thue'
        ]);

        Channel::create([
            'name' => 'Tìm nhà',
            'description' => 'Tìm nhà trọ, nhà nghỉ, ...',
            'slug' => 'tim-nha'
        ]);


        // factory(Channel::class, 10)->create();
    }
}
