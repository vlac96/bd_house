<?php

use Illuminate\Database\Seeder;
use App\HouseType;

class HouseTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HouseType::create([
            'slug' => 'nha-nguyen-can',
            'name' => 'Nhà Nguyên Căn'
        ]);

        HouseType::create([
            'slug' => 'day-tro',
            'name' => 'Dãy trọ'
        ]);

        HouseType::create([
            'slug' => 'nha-nghi',
            'name' => 'Nhà Nghỉ'
        ]);
    }
}
