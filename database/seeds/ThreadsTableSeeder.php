<?php

use Illuminate\Database\Seeder;
use \App\User;
use \App\Channel;
use \App\Thread;

class ThreadsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $title = 'Boarding house Project demo.';
        $thread = Thread::create([
            'title' => $title,
            'slug' => str_slug($title, '-'),
            'body' => 'Nội dung dành cho demo.',
            'user_id' => User::first()->id,
            'channel_id' => Channel::first()->id
        ]);
        
        factory(Thread::class, 100)->create();

    }
}
