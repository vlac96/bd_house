<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('houses', function (Blueprint $table) {
            $table->foreign('house_type_id')
                    ->references('id')->on('house-types')
                    ->onDelete('cascade');
        });
        Schema::table('threads', function (Blueprint $table) {
            $table->foreign('house_id')
                    ->references('id')->on('houses')
                    ->onDelete('cascade');
        });
        Schema::table('replies', function (Blueprint $table) {
            $table->foreign('house_id')
                    ->references('id')->on('houses')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
