<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Channel::class, function (Faker\Generator $faker) {
    $name = $faker->text(50);
    return [
        'name' => $name,
        'description' => $faker->text(200),        
        'slug' => str_slug($name, '-')
    ];
});

$factory->define(App\Thread::class, function (Faker\Generator $faker) {

    $title = $faker->text(50);

    return [
        'title' => $title,
        'slug' => str_slug($title, '-'),        
        'body' => 'Nội dung này dành cho bài demo. Được tạo ra từ db:seed của Laravel',
        'user_id' => $faker->numberBetween(1,11),
        'channel_id' => $faker->numberBetween(1, 2),
        'last_reply' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
    ];

});

$factory->define(App\Reply::class, function (Faker\Generator $faker) {

    return [
        'body' => 'Bình luận này dành cho bài demo',
        'user_id' => $faker->numberBetween(1,11),
        'thread_id' => $faker->numberBetween(1, 101)
    ];
});

$factory->define(App\House::class, function (Faker\Generator $faker) {

    $name = $faker->text(50);

    return [
        'name' => $name,
        'slug' => str_slug($name, '-'),  
        'address' => $faker->text(50),
        'area' => $faker->numberBetween(1,100),
        'price' => $faker->numberBetween(1,100),
        'price_time_unit' => $faker->text(10),
        'house_type_id' => $faker->numberBetween(1,3),
        'user_id' => $faker->numberBetween(1, 30),
        'other' => $faker->text(50),
        'latitude' => '12.2394639',
        'longitude' => '109.1970608',
    ];
});
