## Tập lệnh cài đặt project
`composer install`
    
`copy .env.example .env`

`php artisan key:generate`
	
`npm install`
    
`npm update`
    
`php artisan migrate`
    
`php artisan db:seed`
    
`php artisan storage:link`
    
## Nếu chạy trên homestead
Cấu hình file Homestead.yaml trỏ tới thư mục project để chạy.

## Nếu chạy trên Xampp hoặc Wamp
Copy vào thư mục chạy (Xampp -> htdocs, Wamp -> www), sau đó vào thư mục project gõ lệnh
`php artisan serve`
sau đó vào đường dẫn http://localhost:8000 để xem sản phẩm.

```php 
Author: 'Phạm Văn Lạc'
```
    