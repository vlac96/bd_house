<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Filters\ThreadFilters;
use App\Events\ThreadCreated;
use App\Events\ThreadDeleted;

class Thread extends Model
{
    protected $guarded = [];
    
    protected $hidden = ['user_id', 'channel_id'];

    protected $events = [
        'created' => ThreadCreated::class,
        'deleted' => ThreadDeleted::class,
        'updated' => ThreadCreated::class
    ];
    
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function house()
    {
        return $this->belongsTo(House::class, 'house_id');
    }

    public function replies()
    {
        return $this->hasMany(Reply::class)->orderBy('created_at', 'desc');
    }

    public function addReply($reply)
    {
        $this->replies()->create($reply);
        $this->update(['last_reply' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')]);
    }

    public function scopeFilter($query, ThreadFilters $filters)
    {
        return $filters->apply($query);
    }
}
