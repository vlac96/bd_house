<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Filters\HomeFilters;
use \App\User;
use \App\HouseType;

class House extends Model
{
    protected $table = 'houses';
    protected $fillable = ['name', 'slug', 'address', 'area', 'house_type_id', 'price', 'price_time_unit', 'user_id', 'other', 'latitude', 'longitude'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function house_type()
    {
        return $this->belongsTo(HouseType::class);
    }

    public function scopeFilter($query, HomeFilters $filters)
    {
        return $filters->apply($query);
    }
}
