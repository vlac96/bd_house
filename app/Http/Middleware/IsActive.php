<?php

namespace App\Http\Middleware;

use \Carbon\Carbon;

use Closure;

/**
 * This middleware check if the user is not banned and therefore can post threads and replies.
 */
class IsActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return  mixed
     */
    public function handle($request, Closure $next)
    {
        if (! auth()->user()->isActive()) {
            if (Carbon::now()->gte(Carbon::parse(auth()->user()->status->until))) {
                return $next($request);
            }
            $days = Carbon::now()->diffInDays(Carbon::parse(auth()->user()->status->until));
            $days = $days > 0 ? $days : 1;
            $template = $days > 1 ? "ngày." : "ngày.";
            $message = "Bạn đã bị khóa trong {$days} {$template}";
            return response(['error' => $message], 403);
        }
        return  $next($request);
    }
}
