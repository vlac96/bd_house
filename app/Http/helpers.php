<?php

/**
 * Create a slug from the thread's title.
 *
 * @param  string $title
 * @return string
 */
function makeSlugFromTitle($title)
{
    $slug = str_slug($title);

    $count = \App\Thread::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();

    return $count ? "{$slug}-{$count}" : $slug;
};

function makeSlugFromName($name, $table) 
{
    $slug = str_slug($name);

    switch ($table) {
        case 'Channel':
            $count = \App\Channel::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
            break;

        case 'value':
            $count = \App\HouseType::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
            break;

        case 'value':
            $count = \App\House::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
            break;
        
        default:
            $count = 0;
            break;
    }

    return $count ? "{$slug}-{$count}" : $slug;
}