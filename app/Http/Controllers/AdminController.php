<?php

namespace App\Http\Controllers;

use App\Thread;
use App\Reply;
use App\User;
use App\Channel;
use App\House;

class AdminController extends Controller
{

    public function deleteThread(Thread $thread)
    {
        $thread->delete();
        return response('Đã xóa!', 200);
    }

    public function deleteChannel(Channel $channel)
    {
        $channel->delete();
        return response('Đã xóa!', 200);
    }

    public function deleteReply(Reply $reply)
    {
        $reply->delete();
        return response('Đã xóa!', 200);        
    }

    public function deleteHouse(House $house)
    {
        $house->delete();
        return response('Đã xóa!', 200);
    }

    public function banUser(User $user)
    {
        $this->validate(request(), [
            'days' => 'required|numeric|min:1|max:6000'
        ]);

        $user->banForDays(request('days'));
        return response("Tài khoản {$user->username} đã bị tạm khóa!", 200);
    }

    public function enableUser(User $user)
    {
        $user->enable();
        return response("Tài khoản {$user->username} đã được mở lại!", 200);
    }
}
