<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\HouseType;

class HouseTypesController extends Controller
{
    public function index()
    {
        return HouseType::all();
    }

    public function create(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|min:1|max:6000',
        ]);

        //Create slug
        $slug = makeSlugFromName($request->input('name'), 'HouseType');

        return HouseType::create([
                    'name' => $request->input('name'), 
                    'slug' => $slug
                ]);
    }
}
