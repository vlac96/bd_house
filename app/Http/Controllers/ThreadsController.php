<?php

namespace App\Http\Controllers;

use \App\Channel;
use \App\House;
use \App\Thread;
use \App\Filters\ThreadFilters;

class ThreadsController extends Controller
{

    public function index(Channel $channel, ThreadFilters $filters, $perPage = 20)
    {
        return $this->getThreads($channel, $filters, $perPage);
    }

    public function show($channel, Thread $thread )
    {   
        return $thread->load('replies.creator.profile', 'replies.house', 'replies.house.house_type', 'creator.profile', 'house', 'house.user', 'house.house_type');
    }

    public function store(Channel $channel)
    {
        $this -> validate(request(), [
                'title' => 'required|max:50|min:2',
                'body' => 'required|min:10'
            ]);

        $thread = $channel->addThread([
            'title' => request('title'),
            'slug' => makeSlugFromTitle(request('title')),
            'body' => request('body'),
            'user_id' => auth()->id(),
            'house_id' => request('house_id'),
            'channel_id' => $channel->id,
            'last_reply' => \Carbon\Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return response($thread->slug, 200);
    }

    protected function getThreads(Channel $channel, ThreadFilters $filters, $perPage)
    {

        $threads = Thread::with('creator.profile', 'channel', 'house')->withCount('replies')
                         ->orderBy('last_reply', 'desc')->filter($filters);

        if ($channel->exists) {
            $threads->where('channel_id', $channel->id);
        }
        
        return $threads->paginate($perPage);
    }

    public function update(Thread $thread)
    {
        $this->validate(request(), ['body' => 'required|min:10']);
        $thread->update(request(['body', 'house_id']));

        return response('Cập nhật thành công', 200);
    }

    public function destroy(Thread $thread)
    {
        $thread->delete();
        return response('Bài viết của bạn đã bị xóa', 200);
    }
}
