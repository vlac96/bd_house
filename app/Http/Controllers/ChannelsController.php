<?php

namespace App\Http\Controllers;

use \App\Channel;
use Illuminate\Http\Request;

class ChannelsController extends Controller
{

    public function index()
    {
        return Channel::all();
    }

    public function create(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|min:1|max:6000',
            'description' => 'required|min:1|max:6000',
        ]);

        //Create slug
        $slug = makeSlugFromName($request->input('name'), 'Channel');

        return Channel::create([
                    'name' => $request->input('name'), 
                    'description' => $request->input('description'), 
                    'slug' => $slug
                ]);
    }
}
