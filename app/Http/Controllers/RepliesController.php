<?php

namespace App\Http\Controllers;

use \App\Thread;
use \App\Reply;

class RepliesController extends Controller
{

    public function index($perPage)
    {
        return Reply::with('creator','thread', 'house', 'thread.channel', 'creator.profile')->latest()->paginate($perPage);
    }

    public function store(Thread $thread)
    {
        $thread->addReply([
            'body' => request('body'),
            'user_id' => auth()->id(),
            'house_id' => request('house_id')
        ]);
    }

    public function destroy(Reply $reply)
    {
        $reply->delete();

        return response('Your reply has been deleted', 200);
    }

    public function update(Reply $reply)
    {
        // $this->validate(request(), ['body' => 'required|min:1']);

        $reply->update(request(['body', 'house_id']));

        return response('Your reply has been updated', 200);
    }
}
