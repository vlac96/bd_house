<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\House;
use \App\HouseType;
use \App\Filters\HomeFilters;

class HousesController extends Controller
{
    public function index(HouseType $houseType, HomeFilters $filters, $perPage = 10)
    {
        return $this->getHouses($houseType, $filters, $perPage);
    }

    public function show($homeType, House $house)
    {
        return $house->load('user.profile', 'user', 'house_type');
    }

    public function create(HouseType $houseType)
    {
        
        $this -> validate(request(), [
            'name' => 'required|min:2',
            'address' => 'required|min:2',
            'area' => 'required|min:1',
            'price' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'other' => 'required'
        ]);
        
        $house = $houseType->addHouse([
            'name' => request('name'),
            'slug' => makeSlugFromName(request('name'), 'House'),
            'address' => request('address'),
            'area' => request('area'),
            'price' => request('price'),
            'price_time_unit' => request('price_time_unit'),
            'house_type_id' => $houseType->id,
            'user_id' => auth()->id(),
            'other' => request('other'),
            'latitude' => request('latitude'),
            'longitude' => request('longitude')
        ]);

        return response($house->slug, 200);
    }

    public function update(Request $request, HouseType $houseType, House $house)
    {
        $this->validate(request(), [
            'name' => 'required|min:2',
            'address' => 'required|min:2',
            'area' => 'required|min:1',
            'price' => 'required'
        ]);
        
        $house->update([
            'name' => request('name'),
            'slug' => makeSlugFromName(request('name'), 'House'),
            'address' => request('address'),
            'area' => request('area'),
            'price' => request('price'),
            'price_time_unit' => request('price_time_unit'),
            'house_type_id' => $houseType->id,
            'other' => request('other'),
            'latitude' => request('latitude'),
            'longitude' => request('longitude')
        ]);

        return response()->json($house);
    }

    public function delete(House $house)
    {
        $house->delete();
        return response('Xóa thành công', 200);
    }

    protected function getHouses(HouseType $houseType, HomeFilters $filters, $perPage = 10)
    {
        $houses = House::with('user.profile', 'user', 'house_type')->orderBy('created_at', 'desc')
                    ->filter($filters);

        if($houseType->exists)
        {
            $houses->where('house_type_id', $houseType->id);
        }

        if($perPage == "*") {
            return $houses->get();
        }

        return $houses->paginate($perPage);
    }
}
