<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HouseType extends Model
{
    protected $table = 'house-types';
    protected $guarded = [];

    /**
    * Get the route key for the model.
    *
    * @return string
    */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function houses()
    {
        return $this->hasMany(House::class);
    }

    public function addHouse($fields)
    {
        return $this->houses()->create($fields);
    }
}
