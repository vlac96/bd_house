<?php

namespace App\Policies;

use App\User;
use App\House;
use Illuminate\Auth\Access\HandlesAuthorization;

class HousePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function update(User $user, House $house)
     {
         return $house->user()->first()->id == $user->id;
     }
}
