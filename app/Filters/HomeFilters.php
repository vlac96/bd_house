<?php
namespace App\Filters;
use App\User;

class HomeFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['by', 'search'];
    /**
     * Filter the query by a user.
     *
     * @param  string $username
     * @return Builder
     */
    protected function by($username)
    {
        $user = auth()->user();
        if($user)
        {
            return $this->builder->where('user_id', $user->id);
        }
    }

    /**
    * Search address of house
    * 
    * @param string $terms
    * @return Builder
    */
    public function search($terms)
    {
        $terms = strtolower($terms);
        $terms = explode(',', $terms.trim());
        $builder = $this->builder;
        $builder->where( function($query) use($terms){
            foreach ($terms as $term) {
                if($term) $query -> where('address', 'like', "%{$term}%");
            }
        });

        return $builder;
    }
}